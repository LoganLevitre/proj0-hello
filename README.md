Author: Logan Levitre, UO Junior CIS Major

Contact: loganlevitre@gmail.com OR llevitre@uoregon.edu

# Proj0-Hello
-------------

Trivial project to exercise version control, turn-in, and other
mechanisms.

prints the message "Hello World"

## Instructions:
---------------
--Must have a credentials.ini file to work--


clone repository off bitbucket and download credientials.ini from canvas


add credentials.ini to hello folder


enter make run into console to test
 
